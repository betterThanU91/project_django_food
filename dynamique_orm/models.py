import datetime

from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
from model_utils import FieldTracker
from django.db.models import Sum
from django.contrib.auth.models import User


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class Dish(models.Model):
    class Meta:
        verbose_name = 'Plat'

    name_text = models.CharField('Nom du plat',max_length=200)
    description_text = models.CharField('Description',max_length=500)
    price = models.DecimalField('Prix',max_digits=7, decimal_places=4)
    picture = models.ImageField(upload_to = '', default = 'default.jpg')

    def __str__(self):
        return self.name_text

    def get_picture(self):
        return u'<img src="%s" width="300"/>' % self.picture.url

    get_picture.short_description = 'Suggestion de présentation'
    get_picture.allow_tags = True

    def get_suggested_price(self):
        sum = 0
        for receipe in Receipe.objects.filter(dish_id = self.id):
            sum = sum + receipe.quantity * receipe.ingredient.getprice()
        #Il faut bien faire un petit peu de marge =)
        return float(sum) * 1.5

    def is_available(self):
        is_available = 1
        for receipe in Receipe.objects.filter(dish_id = self.id):
            if receipe.quantity > receipe.ingredient.quantity :
                is_available = 0
        return is_available

    get_suggested_price.short_description = 'Prix de vente conseillé '

    #Le nombre de plat disponnible
    def count_available(self):
        count_available = 99999
        for receipe in Receipe.objects.filter(dish_id = self.id):
            if receipe.ingredient.quantity//receipe.quantity < count_available :
                count_available = receipe.ingredient.quantity//receipe.quantity
        return count_available

    count_available.short_description = 'Nombre de plat disponnible '

class Financial(models.Model):
    class Meta:
        verbose_name = 'Flux financier'

    beneficiary = models.IntegerField()
    amount = models.DecimalField('Montant',max_digits=9, decimal_places=4)
    paid_date = models.DateField('Date du transfert',auto_now_add=True, blank=True)
    description_text = models.CharField('Description',max_length=500)

    def __str__(self):
        return self.description_text

    def getamount(self):
        beneficiary = self.beneficiary
        if beneficiary == 1:
            return u'<b style="color: #FF0000;">-%s</b>' % self.amount
        return u'<b style="color: #00FF00;">%s</b>' % self.amount

    getamount.allow_tags = True
    getamount.short_description = 'Montant'

class Ingredient(models.Model):
    class Meta:
        verbose_name = 'Ingrédient'

    name_text = models.CharField('Nom de l\'ingrédient',max_length=200)
    price = models.DecimalField('Prix',max_digits=7, decimal_places=4)
    quantity = models.IntegerField('Quantité en stock',validators=[MaxValueValidator(999999),MinValueValidator(0)])
    unit_type = models.IntegerField('Type d\'unité')
    #Pour comparer les valeurs et facturer les achats/ ventes
    tracker = FieldTracker()

    def __str__(self):
        return self.name_text

    def getquantity(self):
        return self.quantity

    getquantity.short_description = 'Quantité en stock'

    def getunitype(self):
        unit_type = self.unit_type
        if unit_type == 0:
            return "Unité(s)"
        elif unit_type == 1:
            return "Gramme(s)"
        elif unit_type == 2:
            return "Centilitre(s)"

    getunitype.short_description = 'Unité'

    def getprice(self):
        return self.price

    def save(self):
        previous = self.tracker.previous('quantity')
        current = self.quantity
        if self.tracker.has_changed('quantity'):
            total = Financial.objects.filter(beneficiary=0).aggregate(sum=Sum('amount'))['sum'] - Financial.objects.filter(beneficiary=1).aggregate(sum=Sum('amount'))['sum']
            cost = abs(current - previous) * float(self.price)
            if (current - previous) > 0:
                #Facturation
                Financial.objects.create(beneficiary=1,amount=cost,description_text="Achat de "+str(abs(current - previous))+" "+self.getunitype()+" de "+self.name_text)
                if(total < cost) :
                    print("Attention la banque va pas apprécier")
            else:
                Financial.objects.create(beneficiary=0,amount=cost,description_text="Vente de "+str(abs(current - previous))+" "+self.getunitype()+" de "+self.name_text)
        models.Model.save(self)

class Receipe(models.Model):
    class Meta:
        verbose_name = 'Recette'
        verbose_name_plural = 'Recette'

    dish = models.ForeignKey(Dish, on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    quantity = models.IntegerField('Quantité necessaire')

    def __str__(self):
        return ""

    def getquantity(self):
        if self.ingredient.getquantity() >= self.quantity:
            return u'<b style="color: #00FF00;">%s</b>' % self.ingredient.getquantity()
        return u'<b style="color: #FF0000;">%s</b>' % self.ingredient.getquantity()

    getquantity.short_description = 'Quantité en stock'
    getquantity.allow_tags = True 

    def getunitype(self):
        return self.ingredient.getunitype()

    getunitype.short_description = 'Unité'

class Order(models.Model):
    class Meta:
        verbose_name = 'Commande'

    user = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name="Login du client")
    dish = models.ForeignKey(Dish, on_delete=models.CASCADE,verbose_name="Nom du plat")
    quantity = models.IntegerField('Quantité commandée')
    order_date = models.DateField('Date de la commande',auto_now_add=True, blank=True)
    is_validate = models.IntegerField(default=0)

    def getstatus(self):
        if self.is_validate == 0 :
            return "En attente de validation"
        return "Payée"
    
    getstatus.short_description='Statut de la commande'

    def save(self):
        #current = self.quantity
        previous_order = Order.objects.filter(user_id=self.user.id).filter(is_validate=0).filter(dish=self.dish)
        if previous_order:
            previous_order.update(quantity=previous_order[0].quantity + int(self.quantity))
        else :
            models.Model.save(self)
