from django.apps import AppConfig


class DynamiqueOrmConfig(AppConfig):
    name = 'dynamique_orm'
