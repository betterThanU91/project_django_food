from django.conf.urls import url
from django.contrib.auth.views import login

from . import views

app_name = 'dynamique_orm'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^details/(?P<dish_id>[0-9]+)/$', views.details, name='details'),
    url(r'^order/(?P<dish_id>[0-9]+)/$', views.order, name='order'),
    url(r'^get_count_order/$', views.get_count_order, name='count_order'),
    url(r'^basket/$', views.BasketView.as_view(), name='basket'),
    url(r'^delete_order/(?P<order_id>[0-9]+)/$', views.delete_order, name='delete_order'),
    url(r'^validate_basket/$', views.validate_basket, name='validate_basket'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', views.logout_page, name='logout'),
    url(r'^register/$', views.register_page, name='register'),
]
