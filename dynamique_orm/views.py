from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth import logout
from dynamique_orm.forms import *
from django.template import RequestContext
from django.http import JsonResponse

from .models import *

class IndexView(generic.ListView):
    template_name = 'dynamique_orm/index.html'
    context_object_name = 'list_of_dishes'

    def get_queryset(self):
        return Dish.objects.order_by('-name_text')

class BasketView(generic.ListView):
    context_object_name = 'list_of_orders'
    template_name = 'dynamique_orm/basket.html'

    def get_queryset(self):
	    return Order.objects.filter(user_id=self.request.user.id).filter(is_validate=0).order_by('-order_date')

def details(request,dish_id):
    dish = get_object_or_404(Dish, pk=dish_id)
    return render(request,'dynamique_orm/details.html',context={"dish": dish})

def order(request,dish_id):
    quantity = request.POST['quantity']
    if is_available_dish(dish_id,quantity) and int(quantity) > 0:
        order = Order(user_id=request.user.id,dish_id=dish_id,quantity=quantity)
        order.save()
        set_ingredients(dish_id,quantity)
    return HttpResponseRedirect(reverse('dynamique_orm:index', args=()))

#Web-service AJAX sécurisé par CSRF-TOKEN par Django
def get_count_order(request):
    count_order = 0
    if request.method == 'POST':
        response_data ={}
        count_order = Order.objects.filter(user_id=request.POST['user']).filter(is_validate=0).count()

    response_data['count_order'] = count_order

    return JsonResponse(response_data)

def delete_order(request,order_id):
    if request.method == 'POST':
        response_data ={}
        order = Order.objects.get(pk=order_id)
        if order :
            free_ingredients(order.dish_id,order.quantity)
            order.delete()

    return HttpResponseRedirect(reverse('dynamique_orm:basket', args=()))

def validate_basket(request):
    if request.method == 'POST':
        response_data ={}
        orders = Order.objects.filter(user_id=request.user.id).filter(is_validate=0)
        for order in Order.objects.filter(user_id=request.user.id).filter(is_validate=0):
            Financial.objects.create(beneficiary=0,amount=(order.quantity * order.dish.price),description_text="Vente de "+str(order.quantity)+" "+str(order.dish.name_text)+" à "+request.user.username)
            Order.objects.filter(pk = order.id).update(is_validate = 1)            
            #order.update(is_validate=1)
            print("okay")

    return HttpResponseRedirect(reverse('dynamique_orm:index', args=()))

def logout_page(request):
    logout(request)
    return HttpResponseRedirect(reverse('dynamique_orm:login', args=()))

def register_page(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],password=form.cleaned_data['password1'],email=form.cleaned_data['email'],first_name=form.cleaned_data['first_name'],last_name=form.cleaned_data['last_name'])
            return HttpResponseRedirect(reverse('dynamique_orm:login', args=()))
    form = RegistrationForm()
    return render(request, 'registration/register.html', {'form': form})

def is_available_dish(dish_id,quantity=1):
    is_available = 1
    for receipe in Receipe.objects.filter(dish_id = dish_id):
        if receipe.quantity > receipe.ingredient.quantity * int(quantity) :
            is_available = 0
    return is_available

def set_ingredients(dish_id,quantity):
    for receipe in Receipe.objects.filter(dish_id = dish_id):
        ingredient = Ingredient.objects.filter(pk=receipe.ingredient_id)
        if ingredient :
            ingredient.update(quantity=(ingredient[0].quantity - int(quantity) * receipe.quantity))

def free_ingredients(dish_id,quantity):
    for receipe in Receipe.objects.filter(dish_id = dish_id):
        ingredient = Ingredient.objects.filter(pk=receipe.ingredient_id)
        if ingredient :
            ingredient.update(quantity=(ingredient[0].quantity + int(quantity) * receipe.quantity))
