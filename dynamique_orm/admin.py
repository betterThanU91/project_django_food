from django.contrib import admin

from .models import *
from django.db.models import Sum
from django.shortcuts import redirect

class ReceipeInline(admin.TabularInline):
    model = Receipe
    extra = 3
    fieldsets = [(None,{'fields':['ingredient','quantity','getquantity','getunitype']}),]
    readonly_fields = ['getquantity','getunitype',]

class DishAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name_text']}),
        ('Prix du plat', {'fields': ['get_suggested_price','price']}),
        ('Description du plat', {'fields': ['description_text']}),
        ('Aperçu du plat',{'fields':['picture']}),
    ]

    readonly_fields = ['get_suggested_price',]
    list_display = ('name_text', 'price', 'description_text','get_picture')

    inlines = [ReceipeInline]
    search_fields = ['name_text']

class IngredientAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name_text']}),
        ('Prix de l\'ingrédient à l\'unité (todo cl ou gramme ou unité)', {'fields': ['price']}),
        ('Quantité en stocke', {'fields': ['quantity']}),
        ('Type d\'unité', {'fields': ['unit_type']}),
    ]
    list_display = ('name_text', 'price', 'quantity','getunitype',)
    search_fields = ['name_text']

class FinancialAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['description_text']}),
        ('Montant', {'fields': ['amount']}),
        ('Date', {'fields': ['paid_date']}),
	    ('Description', {'fields': ['description_text']}),
    ]
    readonly_fields = ['description_text','amount','paid_date','description_text',]
    list_display = ('description_text', 'getamount', 'paid_date')
    list_filter = ['paid_date']
    search_fields = ['description_text']
    
    def has_add_permission(self, request):
        return False

class OrderAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Utilisateur', {'fields': ['user']}),
        ('Plat', {'fields': ['dish']}),
        ('Quantité', {'fields': ['quantity']}),
        ('Date de création', {'fields': ['order_date']}),
        ('Statut de validation', {'fields': ['getstatus']}),
    ]
    readonly_fields = ['user','dish','quantity','order_date','getstatus',]
    list_display = ('user', 'dish', 'quantity','order_date','getstatus')
    list_filter = ['order_date']
    search_fields = ['user']

    def has_add_permission(self, request):
        return False

admin.site.register(Dish,DishAdmin)
admin.site.register(Ingredient,IngredientAdmin)
admin.site.register(Financial,FinancialAdmin)
admin.site.register(Order,OrderAdmin)
